from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(TipoConsumo)
admin.site.register(Moneda)
admin.site.register(Servicio)
admin.site.register(Consumo)
admin.site.register(Periocidad)
admin.site.register(ConsumoServicios)
admin.site.register(PagoCunsumoServicio)
