from django.db import models
from custodio.models import Empresa
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
# Create your models here.


class TipoConsumo(models.Model):
    nombre = models.CharField(max_length=30)
    shortname = models.CharField(max_length=5)
    descripcion = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.nombre


class Moneda(models.Model):
    tipo = models.CharField(max_length=30)
    iso = models.CharField(max_length=5)
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.iso


class Servicio(models.Model): # Tipo de servicio y costo fijo o variable
    nombre = models.CharField(max_length=30)
    costo = models.IntegerField(null=True)
    variable = models.BooleanField
    moneda = models.ForeignKey(Moneda)
    porDefecto = models.BooleanField()
    descripcion = models.CharField(max_length=200)
    PrestadorNombre = models.CharField(max_length=50)
    PrestadorFono = models.CharField(null=True, max_length=9)
    PrestadorEmail = models.CharField(null=True, max_length=30)
    PrestadorNroCuenta = models.CharField(null=True, max_length=12)
    PrestadorRazonSocial = models.CharField(null=True, max_length=70)
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.nombre


class Consumo(models.Model):
    descriptor = models.CharField(max_length=30)
    codigo = models.CharField(max_length=20, null=True)
    tipoConsumo = models.ForeignKey(TipoConsumo)
    servicios = models.ManyToManyField(Servicio, through='ConsumoServicios')
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.descriptor


class Periocidad(models.Model):
    nombre = models.CharField(max_length=30)
    periodo = models.IntegerField()  # cantidad de meses del periodo (ej. Mensual periodo = 1, trimestral periodo = 2)
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.nombre


class ConsumoServicios(models.Model):
    consumo = models.ForeignKey(Consumo)
    servicio = models.ForeignKey(Servicio)
    is_mensual = models.BooleanField()  # si es mensual entonces especifica si es mensual, trimestral, anual
    periocidad = models.ForeignKey(Periocidad, null=True)
    dia_mes_pago = models.IntegerField(null=True) # si es mensual, trimestral o anual, debe especificar el dia de pago o dia de facturacion
    is_dias = models.BooleanField()  # en cambio si es por dias especificar los dias del periodo para el pago
    periocidad_dias = models.IntegerField(null=True)
    fecha_ini = models.DateField()
    fecha_ultimo = models.DateField(null=True)

    def __str__(self):
        return self.consumo.descriptor + ' - ' + self.servicio.nombre


class Parametro(models.Model):  ##PARAMETROS PARA CONSUMOS DE SERVICIOS
    alerta_dias = models.IntegerField()
    correo_alerta = models.CharField(max_length=30)
    empresa = models.ForeignKey(Empresa)


class PagoCunsumoServicio(models.Model):
    monto_pagado = models.IntegerField()
    is_pagado = models.BooleanField()
    fecha_pago = models.DateField()
    comentario = models.CharField(max_length=40, null=True)
    consumo_servicio = models.ForeignKey(ConsumoServicios)
