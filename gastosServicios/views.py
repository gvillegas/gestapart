from django.shortcuts import render
from gastosServicios.models import *
from django.contrib.auth.decorators import login_required
# Create your views here.

def Servicios(request):
    coleccionServicios = Servicio.objects.filter(empresa__exact=request.session['empresaid'])
    return render(request, 'Gastos_servicios/servicios.html', {'coleccionServicios': coleccionServicios})


def TiposGastos(request):
    return render(request, 'Gastos_servicios/tipos_gastos.html', {'test':'test'})


def ConfigurarAlertas(request):
    return render(request, 'Gastos_servicios/configurar_alertas.html', {'test':'test'})


def Moneda(request):
    return render(request, 'Gastos_servicios/monedas.html', {'test':'test'})