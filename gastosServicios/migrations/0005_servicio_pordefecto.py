# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('gastosServicios', '0004_auto_20170530_2058'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicio',
            name='porDefecto',
            field=models.BooleanField(),
            preserve_default=False,
        ),
    ]
