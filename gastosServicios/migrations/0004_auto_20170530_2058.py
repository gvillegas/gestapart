# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.timezone import utc
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gastosServicios', '0003_pagocunsumoservicio_parametro'),
    ]

    operations = [
        migrations.AddField(
            model_name='consumoservicios',
            name='fecha_ini',
            field=models.DateField(default=datetime.datetime(2017, 5, 30, 20, 59, 16, 536654, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='consumoservicios',
            name='isDias',
            field=models.BooleanField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='consumoservicios',
            name='isMensual',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]
