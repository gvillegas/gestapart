# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('gastosServicios', '0005_servicio_pordefecto'),
    ]

    operations = [


        migrations.AddField(
            model_name='pagocunsumoservicio',
            name='consumo_servicio',
            field=models.ForeignKey(default=1, to='gastosServicios.ConsumoServicios'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pagocunsumoservicio',
            name='fecha_pago',
            field=models.DateField(default=datetime.datetime(2017, 5, 30, 21, 22, 2, 387871, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pagocunsumoservicio',
            name='is_pagado',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]
