# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20170504_1222'),
        ('gastosServicios', '0002_auto_20170504_1232'),
    ]

    operations = [
        migrations.CreateModel(
            name='PagoCunsumoServicio',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('montoPagado', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Parametro',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('alertadias', models.IntegerField()),
                ('empresa', models.ForeignKey(to='base.Empresa')),
            ],
        ),
    ]
