from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.auth.views import login
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from datetime import datetime, timedelta
from gestapart import forms as gestapartForms
from .models import *
from gestapart.forms import *


def custom_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return login(request, template_name='login.html')


@login_required
def index(request):
    userempresa = UserEmpresa.objects.get(user__exact=request.user.id)
    empresa = userempresa.empresa.nombre
    empresaid = userempresa.empresa.id
    if 'id_usuario' not in request.session:
        request.session['id_usuario'] = request.user.id
    if 'empresa' not in request.session:
        request.session['empresa'] = empresa
    if 'empresaid' not in request.session:
        request.session['empresaid'] = empresaid
    if 'username' not in request.session:
        request.session['username'] = request.user.username
    if 'imageurl' not in request.session:
        request.session['imageurl'] = Empresa.objects.get(pk=empresaid).image.url

    # CUSTODIO STATS
    pendientes_entrega = Custodio.objects.filter(entregado__exact=False, empresa__exact=empresaid,
                                                 extravio__exact=0).count()
    extraviados = Custodio.objects.filter(entregado__exact=False, empresa__exact=empresaid, extravio__exact=1).count()
    historicoTotal = Custodio.objects.filter(empresa__exact=empresaid).count()
    entregadosHoy = Custodio.objects.filter(empresa__exact=empresaid, fecha_entrega__range=[datetime.today().date(),
                                                                                            datetime.today().date() + timedelta(
                                                                                                days=1)]).count()
    ingresadosHoy = Custodio.objects.filter(empresa__exact=empresaid, fecha_ingreso__range=[datetime.today().date(),
                                                                                            datetime.today().date() + timedelta(
                                                                                                days=1)]).count()

    return render(
        request,
        'index.html',
        context={'pendientes_entrega': pendientes_entrega, 'extraviados': extraviados, 'historicoTotal': historicoTotal,
                 'entregadosHoy': entregadosHoy, 'ingresadosHoy': ingresadosHoy}
    )


@login_required
def createUser(request):
    currentUser = User.objects.get(pk=request.user.id)
    if currentUser.has_perm('auth.add_user'):
        if request.method == 'POST':
            if 'password2' in request.POST:
                form = UserAdminForm(request.POST)
                if form.is_valid():
                    try:
                        newuser = form.save()
                        usuarioEmpresa = UserEmpresa.objects.create(user=newuser, empresa=Empresa.objects.get(
                            pk=request.session['empresaid']))
                        usuarioEmpresa.save()
                        messages.success(request, 'Usuario agregado correctamente')
                    except Exception:
                        messages.error(request, 'No se pudo agregar el usuario.')

                    return HttpResponseRedirect(reverse_lazy('AdminUsuarios'))
                else:
                    usersAuth = User.objects.filter(userempresa__empresa_id=request.session['empresaid'],
                                                    is_active=True)
                    userForm = gestapartForms.UserAdminForm()
                    return render(request, 'AdminUsuarios.html',
                                  context={'Usuarios': usersAuth, 'userForm': userForm, 'form': form})

            else:
                print(request.POST, request.FILES)
                form = imageEmpresaSaveForm(request.FILES)
                if form.is_valid():
                    ## ESTO ESTA MALO NO FUNCIONA EL FORM DE LA IMAGEN
                    print('OK LISTO')


        datosEmpresa = Empresa.objects.get(pk=request.session['empresaid'])
        usersAuth = User.objects.filter(userempresa__empresa_id=request.session['empresaid'], is_active=True)
        userForm = gestapartForms.UserAdminForm()
        empresaForm = gestapartForms.imageEmpresaSaveForm()
        return render(request, 'AdminUsuarios.html', context={'Usuarios': usersAuth, 'userForm': userForm, 'empresaForm': empresaForm, 'datosEmpresa': datosEmpresa})
    else:
        return HttpResponseRedirect(reverse_lazy('index'))
