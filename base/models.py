from django.db import models
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser

# Create your models here.

class Empresa(models.Model):
    razonSocial = models.CharField(max_length=200)
    nombre = models.CharField(max_length=120)
    rut = models.CharField(max_length=10)
    image = models.FileField(upload_to='static/uploads/')
    abreviado = models.CharField(max_length=5)
    direccion = models.CharField(max_length=200)
    contactoTecnico = models.CharField(max_length=120, null=True)
    emailContactoTecnico = models.CharField(max_length=120, null=True)
    contactoComercial = models.CharField(max_length=120)
    emailContactoComercial = models.CharField(max_length=120)
    fono = models.CharField(max_length=10)

    def __str__(self):
        return self.nombre


class Sexo(models.Model):
    genero = models.CharField(max_length=30)

    def __str__(self):
        return self.genero


class Pais(models.Model):
    iso = models.CharField(max_length=2)
    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Paises"



class UserEmpresa(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False)
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.user.username + ' - ' + self.empresa.nombre