from django import forms
from django.forms import ModelForm, inlineformset_factory
from custodio.models import *
from django.contrib.auth.forms import UserCreationForm

class ZonaForm(forms.ModelForm):

    class Meta:
        model = Zona
        fields = ['zona_nombre', 'capacidad', 'empresa']

class TiposEquipajesForm(forms.ModelForm):
    class Meta:
        model = EquipajeTipo
        fields = ['nombre', 'tamano', 'empresa']

class TiposCustidiaForm(forms.ModelForm):
    class Meta:
        model = CustodiaTipo
        fields = ['nombre', 'descripcion', 'empresa']

class CustodioForm(forms.ModelForm):
    class Meta:
        model = Custodio
        fields = ['pasajero', 'fecha_ingreso', 'comentario', 'custodia_tipo_id', 'extravio']

class EquipajesForm(forms.ModelForm):
    class Meta:
        model = Equipaje
        fields = ['equipaje_tipo_id', 'color', 'zona_id', 'comentario']
        exclude = ()

class DeleteCustodioForm(forms.ModelForm):
    class Meta:
        model = Custodio
        fields = ['id']

class UserAdminForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = {'username', 'first_name', 'last_name', 'email', 'password1', 'password2'}


EquipajesFormSet = inlineformset_factory(Custodio, Equipaje, form=EquipajesForm, extra=1)

class imageEmpresaSaveForm(forms.ModelForm):
    image = forms.FileField()
    class Meta:
        model = Empresa
        fields = {'image'}