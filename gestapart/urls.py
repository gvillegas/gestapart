"""gestapart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from custodio import views as custodioView
from gastosServicios import views as GSView
from base import views as baseView
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', baseView.index, name='index'),
    url(r'^$', baseView.custom_login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^Zona/', custodioView.Zona, name='Zona'),
    url(r'^TiposEquipajes/', custodioView.TiposEquipajesMantanedor, name='TiposEquipajes'),
    url(r'^TiposCustodia/', custodioView.TiposCustodiaMantenedor, name='TiposCustodia'),
    url(r'^Custodio/Ingresar', custodioView.IngresarCustodio, name='Custodio/Ingresar'),
    url(r'PdfView', custodioView.PdfForge, name='PdfView'),
    url(r'PendientesEntrega', custodioView.PendientesEntrega, name='PendientesEntrega'),
    url(r'^Custodio/Custodio', custodioView.VerEditarCustodio, name='Custodio/Custodio'),
    url(r'Extravios', custodioView.Extravios, name='Extravios'),
    url(r'Historico', custodioView.Historico, name='Historico'),
    url(r'^valida_dcto/', custodioView.validaDocumento, name='valida_dcto'),
    url(r'^AdminUsuarios/', baseView.createUser, name='AdminUsuarios'),
    url(r'^Custodio/Informes', custodioView.Informes, name='Custodio/Informes'),
    url(r'^Gastos_servicios/servicios', GSView.Servicios, name='Gastos_servicios/servicios'),
    url(r'^Gastos_servicios/tipos_gastos', GSView.TiposGastos, name='Gastos_servicios/tipos_gastos'),
    url(r'^Gastos_servicios/configurar_alertas', GSView.ConfigurarAlertas, name='Gastos_servicios/configurar_alertas'),
    url(r'^Gastos_servicios/moneda', GSView.Moneda, name='Gastos_servicios/moneda'),
]
