from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Custodio)
admin.site.register(Equipaje)
admin.site.register(EquipajeTipo)
admin.site.register(CustodiaTipo)
admin.site.register(Zona)
admin.site.register(UserEmpresa)
admin.site.register(Empresa)
admin.site.register(Pasajero)
admin.site.register(Pais)
admin.site.register(TipoDocumento)
admin.site.register(Sexo)
