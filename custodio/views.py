# -*- coding: utf-8 -*-
import pytz as pytz
from io import BytesIO
from django.http import HttpResponse
from django.http import JsonResponse
import random
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.lib import colors
from reportlab.graphics.barcode import code39, code128, code93
from django.contrib.staticfiles.templatetags.staticfiles import static
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import reportlab
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.auth.views import login
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib import messages
from datetime import datetime, timedelta
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from flask import json
from reportlab.pdfgen import canvas
from datetime import date
from custodio.models import Zona as ZonaModel  # cuidado con usar metodos iguales a los models
from gestapart.forms import *
from custodio.models import EquipajeTipo as TiposEquipajesModel
from custodio.models import Custodio as CustodioModel
from custodio.models import Equipaje as EquipajeModel
from custodio.models import CustodiaTipo as TipoCustodiaModel


@login_required
def Zona(request):
    if 'id_usuario' in request.session:
        if request.method == "POST" and 'zona_nombre' in request.POST:
            post_values = request.POST.copy()  # el post request no es accesible asique se hace la copia para editar
            post_values['empresa'] = request.session['empresaid']
            formzona = ZonaForm(post_values)
            if formzona.is_valid():
                post = formzona.save()
                post.save()
            else:
                messages.error('form no valido')
            return HttpResponseRedirect(reverse_lazy('Zona'))
        elif request.method == 'POST' and 'iddelete' in request.POST:
            if not EquipajeModel.objects.filter(empresa__exact=request.session['empresaid'],
                                                zona_id=request.POST['iddelete']):
                ZonaModel.objects.filter(id=request.POST['iddelete']).delete()
                messages.success(request, 'Zona de almacenaje eliminada!')
            else:
                messages.error(request, 'No se pudo eliminar, comprobar que la zona no se ha utilizado por un equipaje')
            return HttpResponseRedirect(reverse_lazy('Zona'))
        else:
            zonas = ZonaModel.objects.filter(empresa__exact=request.session['empresaid'])
            formzona = ZonaForm()
            return render(request, 'Zona.html', {'formzona': formzona, 'zonas': zonas})
    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@login_required
def TiposEquipajesMantanedor(request):
    if 'id_usuario' in request.session:
        if request.method == "POST" and 'tamano' in request.POST:
            post_values = request.POST.copy()
            post_values['empresa'] = request.session['empresaid']
            formTipoEquipaje = TiposEquipajesForm(post_values)
            if formTipoEquipaje.is_valid():
                post = formTipoEquipaje.save()
                post.save()
            else:
                messages.error('form no valido')
            return HttpResponseRedirect(reverse_lazy('TiposEquipajes'))
        elif request.method == 'POST' and 'iddelete' in request.POST:
            if not CustodioModel.objects.filter(empresa__exact=request.session['empresaid'],
                                                equipaje__equipaje_tipo_id__exact=request.POST['iddelete']):
                TiposEquipajesModel.objects.filter(id=request.POST['iddelete']).delete()
                messages.success(request, 'Tipo de equipaje eliminado!')
            else:
                messages.error(request, 'No se puede eliminar, comprobar si esta asignado a algun custodio.')
            return HttpResponseRedirect(reverse_lazy('TiposEquipajes'))
        else:
            formTipoEquipaje = TiposEquipajesForm()
            TiposEquipajes = EquipajeTipo.objects.filter(empresa__exact=request.session['empresaid'])
            return render(request, 'TiposEquipajes.html',
                          {'formTipoEquipaje': formTipoEquipaje, 'TiposEquipajes': TiposEquipajes})
    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@login_required
def TiposCustodiaMantenedor(request):
    if 'id_usuario' in request.session:
        if request.method == "POST" and 'nombre' in request.POST:
            post_values = request.POST.copy()
            post_values['empresa'] = request.session['empresaid']
            formTipoCustodia = TiposCustidiaForm(post_values)
            if formTipoCustodia.is_valid():
                post = formTipoCustodia.save()
                post.save()
            else:
                messages.error('form no valido')
            return HttpResponseRedirect(reverse_lazy('TiposCustodia'))
        elif request.method == 'POST' and 'iddelete' in request.POST:
            if not CustodioModel.objects.filter(empresa__exact=request.session['empresaid'],
                                                custodia_tipo_id=request.POST['iddelete']):
                TipoCustodiaModel.objects.filter(id=request.POST['iddelete']).delete()
                messages.success(request, 'Tipo de custodia eliminada!')
            else:
                messages.error(request, 'No se puede eliminar, comprobar si esta asignado a algun custodio.')
            return HttpResponseRedirect(reverse_lazy('TiposCustodia'))
        else:
            formTipoCustodia = TiposCustidiaForm()
            TiposCustodia = CustodiaTipo.objects.filter(empresa__exact=request.session['empresaid'])
            return render(request, 'TiposCustodia.html',
                          {'formTipoEquipaje': formTipoCustodia, 'TiposCustodia': TiposCustodia})

    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@csrf_exempt
@login_required
def validaDocumento(
        request):  # Aqui solo leeremos el codigo de barras y si se encuentran los datos del objeto pasajero se enviaran
    if request.is_ajax():  # De lo contrario se enviara un 404 para activar los inputs y dejar un mensaje de que no se encontraron datos
        print("llego al view")
        json_dcto = json.loads(request.body)
        documento = ""
        nombre = ""
        apellido = ""
        if ']' or '|' in json_dcto:  # para el caso de los pasaportes
            if ']' in json_dcto:
                arregloTxt = json_dcto.split(']')
                print(arregloTxt)
                documento = arregloTxt[4]
                nombre = arregloTxt[7]
                apellido = arregloTxt[5]

            elif '|' in json_dcto:
                arregloTxt = json_dcto.split('|')
                print(arregloTxt)
                documento = arregloTxt[4]
                nombre = arregloTxt[7]
                apellido = arregloTxt[5]

            # coding: utf-8
            elif 'Ç' in json_dcto:
                arregloTxt = json_dcto.split('Ç')
                documento = arregloTxt[4]
                nombre = arregloTxt[7]
                apellido = arregloTxt[5]

        if 'RUN' in json_dcto:  # para el caso de los carnet
            if '/type' and "'" in json_dcto:
                runIndex = json_dcto.index('RUN')
                typeIndex = json_dcto.index('/type')
                documento = json_dcto[runIndex + 4:typeIndex].replace("'", "-")

            elif '&type' and 'RUN=' in json_dcto:
                runIndex = json_dcto.index('RUN')
                typeIndex = json_dcto.index('&type')
                documento = json_dcto[runIndex + 4:typeIndex]

        if documento != "":
            print(documento)
            try:
                objectPasajero = Pasajero.objects.get(documento__exact=str(documento))
                response = JsonResponse(
                    {'status': 'false', 'message': 'Custodio ingresado correctamente.',
                     'pasajeroId': objectPasajero.id,
                     'pasajeroNombres': objectPasajero.nombres,
                     'pasajeroApellidos': objectPasajero.apellidos,
                     'pasajeroDocumento': objectPasajero.documento,
                     'pasajeroEmail': objectPasajero.email,
                     'pasajeroSexo': objectPasajero.sexo.id,
                     'pasajeroTipoDcto': objectPasajero.tipo_documento.id,
                     'pasajeroTelefono': objectPasajero.telefono},
                    status=200)
            except Pasajero.DoesNotExist:
                response = JsonResponse({'status': 'false',
                                         'message': 'No se encontro pasajero, debe crearlo',
                                         'nombre': nombre,
                                         'documento': documento,
                                         'apellido': apellido},
                                        status=404)
                return response

        else:
            response = JsonResponse({'status': 'false',
                                     'message': 'No se encontro pasajero, debe crearlo',
                                     'nombre': nombre,
                                     'documento': documento,
                                     'apellido': apellido},
                                    status=404)
        return response
    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@csrf_exempt
@login_required
def IngresarCustodio(request):
    if request.method == "POST":
        json_data = json.loads(request.body)
        print(json_data)
        idPasajero = json_data['custodio'][0]['idPasajero']
        txtFechaIn = json_data['custodio'][0]['txtFechaIn']
        txtComentario = json_data['custodio'][0]['txtComentario']
        cboTipoCustodia = json_data['custodio'][0]['cboTipoCustodia']
        checkExtravio = json_data['custodio'][0]['checkExtravio']
        if txtFechaIn != "":
            zonasUsadas = {}
            for equipaje in json_data['equipajes']:  # validar equipaje antes de generar el custodio
                color = equipaje['color']
                # Recopilo zonas para validacion
                if equipaje['zonaEquipaje'] not in zonasUsadas:
                    zonasUsadas[equipaje['zonaEquipaje']] = 1
                else:
                    zonasUsadas[equipaje['zonaEquipaje']] += 1

                if color == "":
                    response = JsonResponse({'status': 'false',
                                             'message': 'Campo Color requerido en equipajes.'},
                                            status=404)
                    return response

            # VALIDO LAS ZONAS y sus capacidades
            for zonatemp in zonasUsadas:
                zonaObjectTemp = ZonaModel.objects.get(pk=int(zonatemp))
                if zonaObjectTemp.capacidad != 0:
                    ocupado = EquipajeModel.objects.filter(zona_id=zonatemp, custodio_id__entregado__exact=0).count()
                    if (zonaObjectTemp.capacidad - ocupado) - zonasUsadas[zonatemp] < 0:
                        response = JsonResponse({'status': 'false',
                                                 'message': 'No queda espacio en una de las zonas (excede: ' + str(
                                                     (zonaObjectTemp.capacidad - ocupado) - zonasUsadas[
                                                         zonatemp]) + ').'},
                                                status=404)
                        return response
            if idPasajero == '':
                txtDocumento = json_data['pasajero'][0]['txtDocumento']
                cboTipoDcto = json_data['pasajero'][0]['cboTipoDcto']
                cboSexo = json_data['pasajero'][0]['cboSexo']
                txtNombres = json_data['pasajero'][0]['txtNombres']
                txtApellidos = json_data['pasajero'][0]['txtApellidos']
                txtEmail = json_data['pasajero'][0]['txtEmail']
                txtTelefono = json_data['pasajero'][0]['txtTelefono']

                if txtDocumento != '':
                    newPasajero = Pasajero.objects.create(nombres=txtNombres,
                                                          apellidos=txtApellidos,
                                                          fullname=txtNombres + ' ' + txtApellidos,
                                                          shortname=txtNombres + ' ' + txtApellidos,
                                                          documento=str(txtDocumento),
                                                          tipo_documento=TipoDocumento.objects.get(pk=cboTipoDcto),
                                                          nacionalidad=Pais.objects.get(pk=46),
                                                          email=txtEmail,
                                                          telefono=txtTelefono,
                                                          sexo=Sexo.objects.get(pk=cboSexo),
                                                          empresa=Empresa.objects.get(pk=request.session['empresaid']))
                    idPasajero = newPasajero.id
                    newPasajero.save()
                else:
                    response = JsonResponse({'status': 'false', 'message': 'Llene la informacion del Pasajero'},
                                            status=404)
                    return response

            txtFechaIn_dateObject = datetime.strptime(txtFechaIn, '%d/%m/%Y %H:%M')
            txtFechaIn_dateObject = pytz.timezone('Iceland').localize(txtFechaIn_dateObject)
            try:
                custodio = CustodioModel.objects.create(pasajero_id=idPasajero,
                                                        fecha_ingreso=txtFechaIn_dateObject,
                                                        comentario=txtComentario,
                                                        usuario_registra=request.session['username'],
                                                        custodia_tipo_id=CustodiaTipo.objects.get(pk=cboTipoCustodia),
                                                        extravio=checkExtravio,
                                                        empresa=Empresa.objects.get(pk=request.session['empresaid']))
            except Exception:
                print('ERROR ACA')

            custodio.save()
            idInsertedCustodio = custodio.id

            for equipaje in json_data['equipajes']:
                tipoEquipaje = equipaje['tipoEquipaje']
                zonaEquipaje = equipaje['zonaEquipaje']
                color = equipaje['color']
                comentario = equipaje['comentario']
                equipajeObject = EquipajeModel.objects.create(color=color,
                                                              comentario=comentario,
                                                              zona_id=ZonaModel.objects.get(pk=zonaEquipaje),
                                                              equipaje_tipo_id=EquipajeTipo.objects.get(
                                                                  pk=tipoEquipaje),
                                                              custodio_id=CustodioModel.objects.get(
                                                                  pk=idInsertedCustodio),
                                                              empresa=Empresa.objects.get(
                                                                  pk=request.session['empresaid']))

                equipajeObject.save()

            detalleEmpresa = Empresa.objects.get(pk=request.session['empresaid'])
            abreviado = detalleEmpresa.abreviado
            codigo_barra = abreviado + str(random.randint(112, 999)) + str(idInsertedCustodio)
            custodio.codigo_barra = codigo_barra
            custodio.save()
            response = JsonResponse(
                {'status': 'false', 'message': 'Custodio ingresado correctamente.', 'idCustodio': idInsertedCustodio},
                status=200)
        else:
            response = JsonResponse({'status': 'false', 'message': 'Llene la informacion de custodia'}, status=404)

        return response
    else:
        tiposDocumentos = TipoDocumento.objects.all()
        sexos = Sexo.objects.all()
        tiposCustodia = TipoCustodiaModel.objects.filter(empresa__exact=request.session['empresaid'])
        tiposEquipaje = TiposEquipajesModel.objects.filter(empresa__exact=request.session['empresaid'])
        zonasEquipaje = ZonaModel.objects.filter(empresa__exact=request.session['empresaid'])
        return render(request, 'Custodio/Ingresar.html', {'tiposCustodia': tiposCustodia,
                                                          'tiposEquipaje': tiposEquipaje,
                                                          'zonasEquipaje': zonasEquipaje,
                                                          'tiposDocumentos': tiposDocumentos,
                                                          'sexos': sexos})


@login_required
def PdfForge(request):
    if 'id' in request.GET:
        try:
            idCustodio = int(request.GET['id'])
            custodio = CustodioModel.objects.get(pk=idCustodio)
        except Exception:
            return HttpResponseRedirect(reverse_lazy('index'))

        if request.session['empresaid'] == custodio.empresa.id:
            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'inline;filename=Ticket-Custodio.pdf"'
            # Create the PDF object, using the response object as its "file."
            pdfmetrics.registerFont(
                TTFont('Monoid', 'custodio/static/Monoid.ttf'))
            # /root/gestapart/gestapart/custodio/static
            p = canvas.Canvas(response)
            # Draw things on the PDF. Here's where the PDF generation happens.
            # See the ReportLab documentation for the full list of functionality..
            custodio = CustodioModel.objects.get(pk=idCustodio)
            p.setTitle('Ticket - Custodio')
            p.setFont('Monoid', 8)
            p.drawString(20, 800, 'DETALLE CUSTODIO - ' + str(custodio.empresa.nombre))
            if custodio.extravio:
                p.drawString(20, 790, '------------***EXTRAVIADO****------------')
            else:
                p.drawString(20, 790, '-----------------------------------------')
            p.drawString(20, 780, 'Cliente: ' + custodio.pasajero.fullname)
            p.drawString(20, 765, 'Equipajes: ' + str(custodio.equipaje_set.count()))
            p.drawString(20, 750, 'Fecha de ingreso: ' + str(custodio.fecha_ingreso.strftime('%d-%m-%Y %H:%M')))
            yintBase = 750
            p.drawString(20, yintBase - 15, 'Equipajes: ')
            yintBase -= 15
            p.drawString(20, yintBase - 15, '-----------------------------------------')
            yintBase -= 35
            for equipaje in EquipajeModel.objects.filter(custodio_id=custodio.id):
                p.drawString(20, yintBase, str(equipaje.equipaje_tipo_id.nombre)
                             + '-' + str(equipaje.equipaje_tipo_id.tamano)
                             + ', ' + (str.lower(equipaje.color))
                             + ', zona: ' + str(equipaje.zona_id.zona_nombre))
                yintBase -= 15
            p.drawString(25, yintBase - 20, '-------------------------------------')
            yintBase -= 20
            p.drawString(100, yintBase - 10, 'Firma')
            yintBase -= 10
            barcode128 = code128.Code128(custodio.codigo_barra, barHeight=25, barWidth=1.5)
            barcode128.drawOn(p, 5, yintBase - 30)
            yintBase -= 30
            p.drawString(100, yintBase - 12, str.upper(custodio.codigo_barra))

            p.showPage()  # Termina la pagina, el output q sigue va en l apg siguiente.
            totalEquipajes = EquipajeModel.objects.filter(custodio_id=custodio.id).count()
            equipajeCount = totalEquipajes
            for equipaje in EquipajeModel.objects.filter(custodio_id=custodio.id):
                p.setFont('Monoid', 8)
                yintBase = 800
                p.drawString(20, yintBase, 'Equipaje - ' + str(custodio.empresa.nombre))
                p.drawString(20, yintBase - 15, '-----------------------------------------')
                yintBase -= 15
                p.drawString(20, yintBase - 15, 'Cliente: ' + str(custodio.pasajero.fullname))
                yintBase -= 15
                p.drawString(20, yintBase - 15, '- Nro: ' + str(equipajeCount) + ' de ' + str(totalEquipajes))
                yintBase -= 15
                p.drawString(20, yintBase - 15, '- Tipo Equipaje: ' + str(equipaje.equipaje_tipo_id.nombre))
                yintBase -= 15
                p.drawString(20, yintBase - 15, '- Tamano Equipaje: ' + str(equipaje.equipaje_tipo_id.tamano))
                yintBase -= 15
                p.drawString(20, yintBase - 15, '- Tamano Equipaje: ' + str.lower(equipaje.color))
                yintBase -= 15
                p.drawString(20, yintBase - 15, '- Lugar almacenaje: ' + str(equipaje.zona_id.zona_nombre))
                yintBase -= 15

                strComentario = str(equipaje.comentario)
                if len(strComentario) > 30:
                    p.drawString(20, yintBase - 15, '- Comentario: ' + strComentario[0:30])
                    yintBase -= 15
                    largoCurrent = len(strComentario) - 30
                    current = 31
                    ini = 31
                    while (largoCurrent > 0):
                        if largoCurrent >= 41:
                            current += 41
                            p.drawString(20, yintBase - 15, strComentario[ini:current])
                            yintBase -= 15
                            ini += 41
                            largoCurrent -= 41
                        else:
                            p.drawString(20, yintBase - 15, strComentario[ini:])
                            yintBase -= 15
                            ini += largoCurrent
                            largoCurrent -= largoCurrent
                else:
                    p.drawString(20, yintBase - 15, '- Comentario: ' + strComentario)
                    yintBase -= 15

                barcode128 = code128.Code128(custodio.codigo_barra, barHeight=25, barWidth=1.5)
                barcode128.drawOn(p, 5, yintBase - 40)
                yintBase -= 40
                p.drawString(100, yintBase - 12, str.upper(custodio.codigo_barra))
                equipajeCount -= 1
                p.showPage()
            p.save()
            return response
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@login_required
def PendientesEntrega(request):
    if request.method == "POST" and "iddel" in request.POST:
        custodioDel = CustodioModel.objects.get(pk=int(request.POST['iddel']))
        if custodioDel.empresa.id == request.session['empresaid']:
            CustodioModel.objects.get(pk=custodioDel.id).delete()
            messages.success(request, 'Custodio eliminado!')
            return HttpResponseRedirect(reverse_lazy('PendientesEntrega'))
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    if 'id_usuario' in request.session:
        pendientesEntrega = CustodioModel.objects.filter(empresa__exact=request.session['empresaid'],
                                                         entregado__exact=0, extravio__exact=0)
        return render(request, 'PendientesEntrega.html', {'pendientesEntrega': pendientesEntrega})

    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@csrf_exempt
@login_required
def VerEditarCustodio(request):
    if 'id_usuario' in request.session:
        if request.method == "POST":  # Si viene por POST es el guardado de los datos
            json_data = json.loads(request.body)
            print(json_data)
            idPasajero = json_data['custodio'][0]['idPasajero']
            txtFechaIn = json_data['custodio'][0]['txtFechaIn']
            txtComentario = json_data['custodio'][0]['txtComentario']
            cboTipoCustodia = json_data['custodio'][0]['cboTipoCustodia']
            checkExtravio = json_data['custodio'][0]['checkExtravio']
            checkEntregado = json_data['custodio'][0]['checkEntregado']
            idCustodio = json_data['custodio'][0]['idCustodio']
            txtDocumento = json_data['pasajero'][0]['txtDocumento']
            cboTipoDcto = json_data['pasajero'][0]['cboTipoDcto']
            cboSexo = json_data['pasajero'][0]['cboSexo']
            txtNombres = json_data['pasajero'][0]['txtNombres']
            txtApellidos = json_data['pasajero'][0]['txtApellidos']
            txtEmail = json_data['pasajero'][0]['txtEmail']
            txtTelefono = json_data['pasajero'][0]['txtTelefono']
            zonasUsadas = {}
            if txtFechaIn != "":
                for equipaje in json_data['equipajes']:  # validar equipaje antes de generar el custodio
                    color = equipaje['color']
                    # Recopilo zonas para validacion
                    if equipaje['zonaEquipaje'] not in zonasUsadas and equipaje['idEquipaje'] == "":
                        zonasUsadas[equipaje['zonaEquipaje']] = 1
                    elif equipaje['idEquipaje'] == "":
                        zonasUsadas[equipaje['zonaEquipaje']] += 1

                    if color == "":
                        response = JsonResponse({'status': 'false',
                                                 'message': 'Campo Color requerido en equipajes.'},
                                                status=404)
                        return response
                # VALIDO LAS ZONAS y sus capacidades
                for zonatemp in zonasUsadas:
                    zonaObjectTemp = ZonaModel.objects.get(pk=int(zonatemp))
                    if zonaObjectTemp.capacidad != 0:
                        ocupado = EquipajeModel.objects.filter(zona_id=zonatemp,
                                                               custodio_id__entregado__exact=0).count()

                        if (zonaObjectTemp.capacidad - ocupado) - zonasUsadas[zonatemp] < 0:
                            response = JsonResponse({'status': 'false',
                                                     'message': 'No queda espacio en una de las zonas (excede: ' + str(
                                                         (zonaObjectTemp.capacidad - ocupado) - zonasUsadas[
                                                             zonatemp]) + ').'},
                                                    status=404)
                            return response

                if idPasajero == '':

                    if txtDocumento != '':
                        newPasajero = Pasajero.objects.create(nombres=txtNombres,
                                                              apellidos=txtApellidos,
                                                              fullname=txtNombres + ' ' + txtApellidos,
                                                              shortname=txtNombres + ' ' + txtApellidos,
                                                              documento=str(txtDocumento),
                                                              tipo_documento=TipoDocumento.objects.get(pk=cboTipoDcto),
                                                              nacionalidad=Pais.objects.get(pk=46),
                                                              email=txtEmail,
                                                              telefono=txtTelefono,
                                                              sexo=Sexo.objects.get(pk=cboSexo),
                                                              empresa=Empresa.objects.get(
                                                                  pk=request.session['empresaid']))
                        idPasajero = newPasajero.id
                        newPasajero.save()
                    else:
                        response = JsonResponse({'status': 'false', 'message': 'Llene la informacion del Pasajero'},
                                                status=404)
                        return response

                else:  # pregunto si hay cambios en el pasajero y guardo los cambios

                    currPasajero = Pasajero.objects.get(pk=idPasajero)
                    if currPasajero.documento != txtDocumento:
                        currPasajero.documento = txtDocumento

                    if currPasajero.sexo.id != cboSexo:
                        currPasajero.sexo = Sexo.objects.get(pk=cboSexo)

                    if currPasajero.nombres != txtNombres:
                        currPasajero.nombres = txtNombres

                    if currPasajero.apellidos != txtApellidos:
                        currPasajero.apellidos = txtApellidos

                    if currPasajero.email != txtEmail:
                        currPasajero.email = txtEmail

                    if currPasajero.telefono != txtTelefono:
                        currPasajero.telefono = txtTelefono

                    if currPasajero.tipo_documento.id != cboTipoDcto:
                        currPasajero.tipo_documento = TipoDocumento.objects.get(pk=cboTipoDcto)

                    if currPasajero.fullname != str(txtNombres + ' ' + txtApellidos):
                        currPasajero.fullname = str(txtNombres + ' ' + txtApellidos)

                    currPasajero.save()

                txtFechaIn_dateObject = datetime.strptime(txtFechaIn, '%d/%m/%Y %H:%M')
                txtFechaIn_dateObject = pytz.timezone('Iceland').localize(
                    txtFechaIn_dateObject)  # iceland para + 00:00 no aletera la hora

                custodio = CustodioModel.objects.get(pk=idCustodio)
                if custodio.pasajero.id != idPasajero:
                    custodio.pasajero = Pasajero.objects.get(pk=idPasajero)

                custodio.comentario = txtComentario
                custodio.entregado = checkEntregado
                if checkEntregado == 1:
                    custodio.fecha_entrega = pytz.timezone('Iceland').localize(datetime.now())
                    custodio.usuario_entrega = request.session['username']
                custodio.custodia_tipo_id = CustodiaTipo.objects.get(pk=cboTipoCustodia)
                custodio.extravio = checkExtravio
                custodio.fecha_ingreso = txtFechaIn_dateObject
                custodio.save()

                for equipaje in json_data['equipajes']:
                    if equipaje['idEquipaje'] != "":
                        idEquipaje = equipaje['idEquipaje']
                        tipoEquipaje = equipaje['tipoEquipaje']
                        zonaEquipaje = equipaje['zonaEquipaje']
                        color = equipaje['color']
                        comentario = equipaje['comentario']
                        equipajeObject = EquipajeModel.objects.get(pk=idEquipaje)
                        equipajeObject.equipaje_tipo_id = EquipajeTipo.objects.get(pk=tipoEquipaje)
                        equipajeObject.zona_id = ZonaModel.objects.get(pk=zonaEquipaje)
                        equipajeObject.color = color
                        equipajeObject.comentario = comentario
                        equipajeObject.save()
                    else:
                        tipoEquipaje = equipaje['tipoEquipaje']
                        zonaEquipaje = equipaje['zonaEquipaje']
                        color = equipaje['color']
                        comentario = equipaje['comentario']
                        equipajeObject = EquipajeModel.objects.create(color=color,
                                                                      comentario=comentario,
                                                                      zona_id=ZonaModel.objects.get(pk=zonaEquipaje),
                                                                      equipaje_tipo_id=EquipajeTipo.objects.get(
                                                                          pk=tipoEquipaje),
                                                                      custodio_id=CustodioModel.objects.get(
                                                                          pk=idCustodio),
                                                                      empresa=Empresa.objects.get(
                                                                          pk=request.session['empresaid']))
                        equipajeObject.save()
                messages.success(request, 'Custodio modificado correctamente.')
                response = JsonResponse({'status': 'false', 'message': 'Custodio modificado correctamente.',
                                         'idCustodio': idCustodio}, status=200)

            else:
                response = JsonResponse({'status': 'false', 'message': 'Llene la informacion de custodia'}, status=404)

            return response
        # si vienen por GET es una vista de los datos.
        if 'id' in request.GET:
            try:
                idCustodio = int(request.GET['id'])
                custodio = CustodioModel.objects.get(pk=idCustodio)
                equipajesCustodio = EquipajeModel.objects.filter(custodio_id=custodio.id)
            except Exception:
                return HttpResponseRedirect(reverse_lazy('index'))
            if request.session['empresaid'] == custodio.empresa.id:

                tiposCustodia = TipoCustodiaModel.objects.filter(empresa__exact=request.session['empresaid'])
                tiposEquipaje = TiposEquipajesModel.objects.filter(empresa__exact=request.session['empresaid'])
                zonasEquipaje = ZonaModel.objects.filter(empresa__exact=request.session['empresaid'])
                tiposDocumentos = TipoDocumento.objects.all()
                sexos = Sexo.objects.all()
                return render(request, 'Custodio/Custodio.html', {'custodio': custodio,
                                                                  'equipajesCustodio': equipajesCustodio,
                                                                  'tiposCustodia': tiposCustodia,
                                                                  'tiposEquipaje': tiposEquipaje,
                                                                  'zonasEquipaje': zonasEquipaje,
                                                                  'tiposDocumentos': tiposDocumentos,
                                                                  'sexos': sexos})

            else:
                return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@login_required
def Extravios(request):
    if request.method == "POST" and "iddel" in request.POST:
        custodioDel = CustodioModel.objects.get(pk=int(request.POST['iddel']))
        if custodioDel.empresa.id == request.session['empresaid']:
            CustodioModel.objects.get(pk=custodioDel.id).delete()
            messages.success(request, 'Custodio eliminado!')
            return HttpResponseRedirect(reverse_lazy('Extravios'))
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    if 'id_usuario' in request.session:
        extraviados = CustodioModel.objects.filter(empresa__exact=request.session['empresaid'],
                                                   entregado__exact=0, extravio__exact=1)
        return render(request, 'Extravios.html', {'extraviados': extraviados})

    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@login_required
def Historico(request):
    if request.method == "POST" and "iddel" in request.POST:
        custodioDel = CustodioModel.objects.get(pk=int(request.POST['iddel']))
        if custodioDel.empresa.id == request.session['empresaid']:
            CustodioModel.objects.get(pk=custodioDel.id).delete()
            messages.success(request, 'Custodio eliminado!')
            return HttpResponseRedirect(reverse_lazy('Extravios'))
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    if 'id_usuario' in request.session:
        historico = CustodioModel.objects.filter(empresa__exact=request.session['empresaid'],
                                                 entregado__exact=1)
        return render(request, 'Historico.html', {'historico': historico})

    else:
        return HttpResponseRedirect(reverse_lazy('index'))


@login_required
def Informes(request):
    if request.method == "POST":
        args = {}
        args['empresa__exact'] = str(request.session['empresaid'])
        print(request.POST)  # DEBUG
        if 'ckRangosFechasIn' in request.POST and request.POST['ckRangosFechasIn'] == 'on':
            args['fecha_ingreso__range'] = [datetime.strptime(request.POST['fecha_desde_in'], '%d/%m/%Y'),
                                            datetime.strptime(request.POST['fecha_hasta_in'], '%d/%m/%Y')]

        if 'ckRangosFechasOut' in request.POST and request.POST['ckRangosFechasOut'] == 'on':
            args['fecha_entrega__range'] = [datetime.strptime(request.POST['fecha_desde_out'], '%d/%m/%Y'),
                                            datetime.strptime(request.POST['fecha_hasta_out'], '%d/%m/%Y')]

        if 'ckEstado' in request.POST and request.POST['ckEstado'] == 'on':
            if request.POST['cboEstado'] == '0':  # no se ha entregado
                args['entregado'] = False
            elif request.POST['cboEstado'] == '1':  # se entrego
                args['entregado'] = True

        if 'ckExtraviado' in request.POST and request.POST['ckExtraviado'] == 'on':
            if request.POST['optExtraviado'] == '0':  # no se ha extraviado
                args['extravio'] = False
            elif request.POST['optExtraviado'] == '1':  # se extravio
                args['extravio'] = True

        if 'ckUserEntrega' in request.POST and request.POST['ckUserEntrega'] == 'on':
            args['usuario_entrega'] = request.POST['cboUsuarioEntrega']

        if 'ckUserRegistra' in request.POST and request.POST['ckUserRegistra'] == 'on':
            args['usuario_registra'] = request.POST['cboUsuarioRegistra']

        if 'ckZonas' in request.POST and request.POST['ckZonas'] == 'on':
            args['equipaje__zona_id'] = request.POST['cboZonas']

        print('## ARGS: ', args)  # DEBUG
        if args.__len__() > 1:
            custodios = Custodio.objects.filter(**args)
            if custodios.count() >= 1:
                response = HttpResponse(content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="Informe-Custodio-' + datetime.now().strftime(
                    '%d-%m-%Y') + '.pdf"'
                buffer = BytesIO()
                # Create the PDF object, using the BytesIO object as its "file."
                p = canvas.Canvas(buffer)
                p.setTitle('Informe Custodios')
                # Utilizamos el archivo logo_django.png que está guardado en la carpeta media/imagenes
                archivo_imagen = request.session['imageurl']
                # Definimos el tamaño de la imagen a cargar y las coordenadas correspondientes
                p.drawImage(archivo_imagen, 40, 750, 120, 90, preserveAspectRatio=True)
                p.setFont("Helvetica", 16)
                # Dibujamos una cadena en la ubicación X,Y especificada
                p.drawString(250, 790, request.session['empresa'])
                p.setFont("Helvetica", 14)
                p.drawString(200, 770, u"INFORME DE CUSTODIOS")
                # Detalles del informe
                p.setFont("Helvetica", 8)
                p.drawString(240, 750, 'Fecha de emision: ' + datetime.now().strftime('%d-%m-%Y %H:%M'))
                encabezados = ('Cod.',
                               'Nombre Pasajero',
                               'Fecha ingreso',
                               'Fecha entrega',
                               'Usr. ingresa',
                               'Usr. entrega',
                               'Equipajes',
                               'Extraviado',
                               'Entregado')
                detalles = [(custodio.codigo_barra,
                             custodio.pasajero.nombres + ' ' + custodio.pasajero.apellidos,
                             custodio.fecha_ingreso.strftime('%d-%m-%Y %H:%M'),
                             custodio.fecha_entrega.strftime('%d-%m-%Y %H:%M') if custodio.fecha_entrega else '-',
                             custodio.usuario_registra,
                             custodio.usuario_entrega if custodio.usuario_entrega else '-',
                             custodio.equipaje_set.count(),
                             'Si' if custodio.extravio else 'No', 'Si' if custodio.entregado else 'No') for custodio in
                            custodios]

                # Establecemos el tamaño de cada una de las columnas de la tabla
                detalle_orden = Table([encabezados] + detalles, colWidths=[2 * cm,
                                                                           3 * cm,
                                                                           2.6 * cm,
                                                                           2.6 * cm,
                                                                           2 * cm,
                                                                           2 * cm,
                                                                           1.8 * cm,
                                                                           1.8 * cm,
                                                                           1.8 * cm])
                detalle_orden.setStyle(TableStyle(
                    [
                        # La primera fila(encabezados) va a estar centrada
                        ('ALIGN', (0, 0), (3, 0), 'CENTER'),
                        # Los bordes de todas las celdas serán de color negro y con un grosor de 1
                        ('GRID', (0, 0), (-1, -1), 1, colors.black),
                        # El tamaño de las letras de cada una de las celdas será de 10
                        ('FONTSIZE', (0, 0), (-1, -1), 8),
                    ]
                ))
                # Establecemos el tamaño de la hoja que ocupará la tabla
                detalle_orden.wrapOn(p, 800, 600)
                # Definimos la coordenada donde se dibujará la tabla
                detalle_orden.drawOn(p, 25, 680)
                detalle_orden.canv = p
                ancho, largo = detalle_orden.wrap(0, 0)
                print(ancho, largo)
                p.setFont("Helvetica", 16)
                # Dibujamos una cadena en la ubicación X,Y especificada
                y = 680 - largo
                p.drawString(250, y, "Resumen")
                y -= 60
                encabezados1 = ('Ingresos',
                               'Entregas',
                               'Equipajes',
                               'Extraviados')

                ingresos = 0
                entregas = 0
                equipajes = 0
                extraviados = 0
                for custodioRs in custodios:
                    ingresos += 1
                    equipajes += custodioRs.equipaje_set.count()
                    if custodioRs.entregado:
                        entregas += 1

                    if custodioRs.extravio:
                        extraviados += 1


                detalles1 = [[ingresos, entregas, equipajes, extraviados]]

                # Establecemos el tamaño de cada una de las columnas de la tabla
                detalle_orden1 = Table([encabezados1] + detalles1, colWidths=[2 * cm,
                                                                           2 * cm,
                                                                           2 * cm,
                                                                           2 * cm])
                detalle_orden1.setStyle(TableStyle(
                    [
                        # La primera fila(encabezados) va a estar centrada
                        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                        # Los bordes de todas las celdas serán de color negro y con un grosor de 1
                        ('GRID', (0, 0), (-1, -1), 1, colors.black),
                        # El tamaño de las letras de cada una de las celdas será de 10
                        ('FONTSIZE', (0, 0), (-1, -1), 9),
                    ]
                ))
                detalle_orden1.wrapOn(p, 800, 600)
                detalle_orden1.drawOn(p, 180, y)

                # Close the PDF object cleanly.
                p.showPage()
                p.save()

                # Get the value of the BytesIO buffer and write it to the response.
                pdf = buffer.getvalue()
                buffer.close()
                response.write(pdf)
                return response
            else:
                messages.warning(request, 'No se encontraron datos con ese filtro.')
                return HttpResponseRedirect(reverse_lazy('Custodio/Informes'))
        else:
            messages.error(request, 'Agregar algun filtro.')
            return HttpResponseRedirect(reverse_lazy('Custodio/Informes'))

    else:
        usuariosEmpresa = User.objects.filter(userempresa__empresa=request.session['empresaid'])
        zonasEmpresa = ZonaModel.objects.filter(empresa__exact=request.session['empresaid'])
        return render(request, 'Custodio/Informes.html', {'usuariosEmpresa': usuariosEmpresa,
                                                          'zonasEmpresa': zonasEmpresa})
