from django.db import models
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from base.models import *
# Create your models here.

class TipoDocumento(models.Model):
    nombre = models.CharField(max_length=30)
    nacionalidad = models.ForeignKey(Pais, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de documentos"


class Pasajero(models.Model):
    nombres = models.CharField(max_length=80)
    apellidos = models.CharField(max_length=80)
    fullname = models.CharField(max_length=160)
    shortname = models.CharField(max_length=80)
    documento = models.CharField(max_length=20, db_index=True)
    tipo_documento = models.ForeignKey(TipoDocumento)
    nacionalidad = models.ForeignKey(Pais)
    email = models.CharField(max_length=80, null=False)
    telefono = models.CharField(max_length=20, null=False)
    sexo = models.ForeignKey(Sexo)
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.shortname

    class Meta:
        verbose_name_plural = "Pasajeros"



class Zona(models.Model):
    zona_nombre = models.CharField(max_length=45, null=False)
    capacidad = models.IntegerField(default=0)
    empresa = models.ForeignKey(Empresa)

    class Meta:
        verbose_name_plural = "Zonas"

    def __str__(self):
        return self.zona_nombre


class EquipajeTipo(models.Model):
    nombre = models.CharField(max_length=45)
    tamano = models.CharField(max_length=45)
    empresa = models.ForeignKey(Empresa)

    class Meta:
        verbose_name_plural = "Tipos equipaje"

    def __str__(self):
        return self.nombre


class CustodiaTipo(models.Model):
    nombre = models.CharField(max_length=45)
    descripcion = models.CharField(max_length=200)
    empresa = models.ForeignKey(Empresa)

    class Meta:
        verbose_name_plural = "Tipos custodia"

    def __str__(self):
        return self.nombre + ' - ' + self.empresa.nombre


class Custodio(models.Model):
    pasajero = models.ForeignKey(Pasajero)
    fecha_ingreso = models.DateTimeField(null=False)
    fecha_entrega = models.DateTimeField(null=True)
    usuario_registra = models.CharField(max_length=125)
    usuario_entrega = models.CharField(max_length=125, null=True)
    comentario = models.CharField(max_length=200, null=True)
    extravio = models.BooleanField()
    custodia_tipo_id = models.ForeignKey(CustodiaTipo)
    codigo_barra = models.CharField(max_length=150, default='')
    entregado = models.BooleanField(default=False)
    empresa = models.ForeignKey(Empresa)

    class Meta:
        verbose_name_plural = "Custodios"

    def __str__(self):
        return self.codigo_barra

class Equipaje(models.Model):
    color = models.CharField(max_length=45)
    comentario = models.CharField(max_length=45, null=True)
    zona_id = models.ForeignKey(Zona, on_delete=models.CASCADE)
    equipaje_tipo_id = models.ForeignKey(EquipajeTipo)
    custodio_id = models.ForeignKey(Custodio) #custodio
    empresa = models.ForeignKey(Empresa)

    def __str__(self):
        return self.color

    class Meta:
        verbose_name_plural = "Equipajes"
