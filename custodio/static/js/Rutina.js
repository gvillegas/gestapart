/**
 * Created by r00ter on 24-03-2017.
 */
function DisEnaRangosFechasIn(t) {
    if ($(t).is(':checked')){
        $('#id_fecha_desde_in').prop('disabled', false);
        $('#id_fecha_hasta_in').prop('disabled', false);
    }else{
        $('#id_fecha_desde_in').prop('disabled', true);
        $('#id_fecha_hasta_in').prop('disabled', true);
    }
}

function DisEnaRangosFechasOut(t) {
    if ($(t).is(':checked')){
        $('#id_fecha_desde_out').prop('disabled', false);
        $('#id_fecha_hasta_out').prop('disabled', false);
    }else{
        $('#id_fecha_desde_out').prop('disabled', true);
        $('#id_fecha_hasta_out').prop('disabled', true);
    }
}

function DisEnaUserRegistra(t) {
    if ($(t).is(':checked')){
        $('#id_cboUsuarioRegistra').prop('disabled', false);
    }else{
        $('#id_cboUsuarioRegistra').prop('disabled', true);
    }
}

function DisEnaUserEntrega(t) {
    if ($(t).is(':checked')){
        $('#id_cboUsuarioEntrega').prop('disabled', false);
    }else{
        $('#id_cboUsuarioEntrega').prop('disabled', true);
    }
}

function DisEnaEstado(t) {
    if ($(t).is(':checked')){
        $('#id_cboEstado').prop('disabled', false);
    }else{
        $('#id_cboEstado').prop('disabled', true);
    }
}

function DisEnaExtraviado(t) {
    if ($(t).is(':checked')){
        $('#íd_rbExtraviado').prop('disabled', false);
    }else{
        $('#íd_rbExtraviado').prop('disabled', true);
    }
}

function DisEnaZonas(t) {
    if ($(t).is(':checked')){
        $('#id_cboZonas').prop('disabled', false);
    }else{
        $('#id_cboZonas').prop('disabled', true);
    }
}

$(document).ready(function () {
        $("#id_fecha_desde_in, #id_fecha_desde_out, #id_fecha_hasta_in, #id_fecha_hasta_out").daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_4",
            timePicker24Hour: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    });


function NuevoEquipaje() {
    var zonas = document.getElementById('zonaEquipaje').innerHTML;
    var tipos = document.getElementById('tipoEquipaje').innerHTML;
    var html = '<form class="added form-horizontal form-label-left"><div class="col-md-4 col-sm-6 col-xs-12 form-group  "><label for="tipoEquipaje">Tipo</label><select name="tipoEquipaje" id="tipoEquipaje" class="form-control">' + tipos + '</select> </div> <div class="col-md-2 col-sm-6 col-xs-12 form-group  "> <label for="color">Color</label> <input type="text" class="form-control" id="color" placeholder=""> </div> <div class="col-md-2 col-sm-6 col-xs-12 form-group  "> <label for="zonaEquipaje">Zona</label> <select id="zonaEquipaje" class="form-control">' + zonas + '</select> </div> <div class="col-md-3 col-sm-6 col-xs-12 form-group  "> <label for="comantario">Comentario</label> <input type="text" class="form-control" id="comentario"></div><div class="col-md-1 col-sm-6 col-xs-12 form-group"><label for="eliminar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><button class="btn btn-dark"  id="eliminar" onclick="delthis(this.form)">X</button></div></form>';
    $('#equipajes').append(html)
}

function cancelarCustodio() {
    $('.added').remove();
    document.getElementById('custodiaInfo').reset();
    document.getElementById('equipajesForm').reset();
}

function EnviarForm() {
    var forms = document.forms;
    var json_data = {
        pasajero: [],
        custodio: [],
        equipajes: []
    };
    var idPasajero = '';
    if ($('#idPasajero').val()!=''){
        idPasajero = $('#idPasajero').val();
    }else{
       var txtDocumento = $('#txtDocumento').val();
       var cboTipoDcto = $('#cboTipoDcto').val();
       var cboSexo = $('#cboSexo').val();
       var txtNombres = $('#txtNombres').val();
       var txtApellidos = $('#txtApellidos').val();
       var txtEmail = $('#txtEmail').val();
       var txtTelefono = $('#txtTelefono').val();

       json_data.pasajero.push({
                "txtDocumento": txtDocumento,
                "cboTipoDcto": cboTipoDcto,
                "cboSexo": cboSexo,
                "txtNombres": txtNombres,
                "txtApellidos": txtApellidos,
                "txtEmail": txtEmail,
                "txtTelefono": txtTelefono
            });

    }

    for (I = 0; I < forms.length; I++) {
        if (forms[I].elements.namedItem('txtFechaIn')) {
            var txtFechaIn = forms[I].elements.namedItem('txtFechaIn').value;
            var txtComentario = forms[I].elements.namedItem('txtComentario').value;
            var cboTipoCustodia = forms[I].elements.namedItem('cboTipo').value;
            console.log('log1');
            var checkExtravio = 0;
            if ($('#extravio').is(':checked')) {
                var checkExtravio = 1;
            }

            var csrfmiddlewaretoken = forms[I].elements.namedItem('csrfmiddlewaretoken').value;
            json_data.custodio.push({
                "txtFechaIn": txtFechaIn,
                "txtComentario": txtComentario,
                "cboTipoCustodia": cboTipoCustodia,
                "checkExtravio": checkExtravio,
                "idPasajero": idPasajero
            });
        }
        if (forms[I].elements.namedItem('tipoEquipaje')) {
            var tipoEquipaje = forms[I].elements.namedItem('tipoEquipaje').value;
            var color = forms[I].elements.namedItem('color').value;
            var zonaEquipaje = forms[I].elements.namedItem('zonaEquipaje').value;
            var comentario = forms[I].elements.namedItem('comentario').value;

            json_data.equipajes.push({
                "tipoEquipaje": tipoEquipaje,
                "color": color,
                "zonaEquipaje": zonaEquipaje,
                "comentario": comentario
            });
        }
    }
    $.ajax({
        url: "", // the endpoint
        type: "POST", // http method
        dataType: 'json',
        data: JSON.stringify(json_data), // data sent with the post request

        // handle a successful response
        success: function (json) {
            $('.added').remove();
            liberarCampos();
            document.getElementById('custodiaInfo').reset();
            document.getElementById('equipajesForm').reset();
            msg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + json.message + '</strong>';
            $('#mensajes-ajax').append(msg);
            console.log(json); // another sanity check
            window.open('../PdfView/?id=' + json.idCustodio, 'Ticket - Custodia');
        },

        // handle a non-successful response
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr)
            msg = '<div class="alert alert-error alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + xhr.responseJSON.message + '</strong>';
            $('#mensajes-ajax').append(msg);
        }
    });
}

$(document).ready(function () {
    $(".search").keyup(function () {
        var searchTerm = $(".search").val();
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

        $.extend($.expr[':'], {
            'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'false');
        });

        $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
        });

        var jobCount = $('.results tbody tr[visible="true"]').length;
        $('.counter').text(jobCount + ' resultado');

        if (jobCount == '0') {
            $('.no-result').show();
        }
        else {
            $('.no-result').hide();
        }
    });
});

function disableAll() {
    $('#custodiaInfo :input').prop("disabled", true);
    $('#equipajes :input').prop("disabled", true);
    $('#EditAgregarEquipaje').prop("disabled", true);
    $('#Editsend').prop("disabled", true);
    $('#custodiaInfo .toggle').prop("disabled", true);
    $('#txtDocumento').prop('disabled', true);
    $('#cboTipoDcto').prop('disabled', true);
    $('#cboSexo').prop('disabled', true);
    $('#txtNombres').prop('disabled', true);
    $('#txtApellidos').prop('disabled', true);
    $('#txtEmail').prop('disabled', true);
    $('#txtTelefono').prop('disabled', true);
    $('#btnLector').prop('disabled', true);
    $('#btnLibera').prop('disabled', true);

}
function enableAll() {
    $('#custodiaInfo .toggle').removeAttr('disabled');
    $('#custodiaInfo :input').removeAttr('disabled');
    $('#equipajes :input').removeAttr('disabled');
    $('#EditAgregarEquipaje').removeAttr('disabled');
    $('#Editsend').removeAttr('disabled');
    $('#txtDocumento').prop('disabled', false);
    $('#cboTipoDcto').prop('disabled', false);
    $('#cboSexo').prop('disabled', false);
    $('#txtNombres').prop('disabled', false);
    $('#txtApellidos').prop('disabled', false);
    $('#txtEmail').prop('disabled', false);
    $('#txtTelefono').prop('disabled', false);
    $('#btnLector').prop('disabled', false);
    $('#btnLibera').prop('disabled', false);
    console.log('habilitando');
}

function toggleClick(t) {
    if ($(t).is(':checked')) {
        enableAll();
    } else {
        disableAll();
    }
}

function EditarCustodioForm() {
    var forms = document.forms;
    var json_data = {
        pasajero: [],
        custodio: [],
        equipajes: []
    };
    var idPasajero = '';
    if ($('#idPasajero').val()!=''){
        idPasajero = $('#idPasajero').val();
    } // en el agregar no envio los datos porque no van a ser comparados
    // aca aun que haya idpasajero debo enviar los datos para actualizar la infomarcion del pasajero
       var txtDocumento = $('#txtDocumento').val();
       var cboTipoDcto = $('#cboTipoDcto').val();
       var cboSexo = $('#cboSexo').val();
       var txtNombres = $('#txtNombres').val();
       var txtApellidos = $('#txtApellidos').val();
       var txtEmail = $('#txtEmail').val();
       var txtTelefono = $('#txtTelefono').val();

       json_data.pasajero.push({
                "txtDocumento": txtDocumento,
                "cboTipoDcto": cboTipoDcto,
                "cboSexo": cboSexo,
                "txtNombres": txtNombres,
                "txtApellidos": txtApellidos,
                "txtEmail": txtEmail,
                "txtTelefono": txtTelefono
            });


    for (I = 0; I < forms.length; I++) {
        if (forms[I].elements.namedItem('txtFechaIn')) {
            var txtFechaIn = forms[I].elements.namedItem('txtFechaIn').value;
            var txtComentario = forms[I].elements.namedItem('txtComentario').value;
            var cboTipoCustodia = forms[I].elements.namedItem('cboTipo').value;
            var txtIdCustodio = forms[I].elements.namedItem('idCustodio').value;

            var checkEntregado = 0;
            if ($('#entregado').is(':checked')){
                checkEntregado = 1;
            }

            var checkExtravio = 0;
            if ($('#extravio').is(':checked')) {
                checkExtravio = 1;
            }


            var csrfmiddlewaretoken = forms[I].elements.namedItem('csrfmiddlewaretoken').value;

            json_data.custodio.push({
                "txtFechaIn": txtFechaIn,
                "txtComentario": txtComentario,
                "cboTipoCustodia": cboTipoCustodia,
                "checkExtravio": checkExtravio,
                "idCustodio": txtIdCustodio,
                "checkEntregado": checkEntregado,
                "idPasajero": idPasajero
            });
        }

        if (forms[I].elements.namedItem('tipoEquipaje')) {
            var tipoEquipaje = forms[I].elements.namedItem('tipoEquipaje').value;
            var color = forms[I].elements.namedItem('color').value;
            var zonaEquipaje = forms[I].elements.namedItem('zonaEquipaje').value;
            var comentario = forms[I].elements.namedItem('comentario').value;
            try {
                var idEquipaje = forms[I].elements.namedItem('idEquipaje').value;
            } catch (e) {
                var idEquipaje = ""
            }
            json_data.equipajes.push({
                "tipoEquipaje": tipoEquipaje,
                "color": color,
                "zonaEquipaje": zonaEquipaje,
                "comentario": comentario,
                "idEquipaje": idEquipaje
            });
        }
    }
    $.ajax({
        url: "", // the endpoint
        type: "POST", // http method
        dataType: 'json',
        data: JSON.stringify(json_data), // data sent with the post request

        // handle a successful response
        success: function (json) {
            location.reload();

        },

        // handle a non-successful response
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr)
            msg = '<div class="alert alert-error alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + xhr.responseJSON.message + '</strong>';
            $('#mensajes-ajax').append(msg);
        }
    });
}
function delthis(t) {
    $(t).remove();
}


function confirmarDel(t) {
       bootbox.confirm("Esta seguro de eliminar esta custodia y sus equipajes?", function(result) {
            if (result) {
                $(t).submit();
            }
});
}

function abrirVentana(t) {
    window.open(t);
}

function liberarCampos() {
    $('#txtDocumento').prop('disabled', false);
    $('#cboTipoDcto').prop('disabled', false);
    $('#cboSexo').prop('disabled', false);
    $('#txtNombres').prop('disabled', false);
    $('#txtApellidos').prop('disabled', false);
    $('#txtEmail').prop('disabled', false);
    $('#txtTelefono').prop('disabled', false);
    $('#txtDocumento').val('');
    $('#cboTipoDcto').val('1').change();
    $('#cboSexo').val('1').change();
    $('#txtNombres').val('');
    $('#txtApellidos').val('');
    $('#txtEmail').val('');
    $('#txtTelefono').val('');
    $('#idPasajero').val('');
}

function lectorCodigos() {
    liberarCampos();
    bootbox.prompt("Leer el documento", function(result){
        $.ajax({
        url: "../valida_dcto/", // url a enviar
        type: "POST", // http method
        dataType: 'json',
        data: JSON.stringify(result), // datos a enviar

        // obteniendo respuesta positiva
        success: function (json) {
            $('#txtDocumento').val(json.pasajeroDocumento);
            $('#cboTipoDcto').val(json.pasajeroTipoDcto).change();
            $('#cboSexo').val(json.pasajeroSexo).change();
            $('#txtNombres').val(json.pasajeroNombres);
            $('#txtApellidos').val(json.pasajeroApellidos);
            $('#txtEmail').val(json.pasajeroEmail);
            $('#txtTelefono').val(json.pasajeroTelefono);
            $('#idPasajero').val(json.pasajeroId);
            $('#txtDocumento').prop('disabled', true);
            $('#cboTipoDcto').prop('disabled', true);
            $('#cboSexo').prop('disabled', true);
            $('#txtNombres').prop('disabled', true);
            $('#txtApellidos').prop('disabled', true);
            $('#txtEmail').prop('disabled', true);
            $('#txtTelefono').prop('disabled', true);
        },
        // obteniendo un erro o respuesta no positiva
        error: function (xhr, ajaxOptions, thrownError) {
            bootbox.alert(xhr.responseJSON.message);
            $('#txtDocumento').val(xhr.responseJSON.documento);
            $('#txtNombres').val(xhr.responseJSON.nombre);
            $('#txtApellidos').val(xhr.responseJSON.apellido);

        }
    });
});
}
