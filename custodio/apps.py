from django.apps import AppConfig


class CustodioConfig(AppConfig):
    name = 'custodio'
